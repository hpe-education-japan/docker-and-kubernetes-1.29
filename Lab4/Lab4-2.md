## Task2<br>匿名ボリューム、名前付きボリューム、外部ボリューム  

**1) 現在のボリュームリストを確認しておきます**
```
$ docker volume ls

DRIVER    VOLUME NAME
local     6f8ec82139165820db09b611a9fe074c29a5500819892706ccb07e83913412da
local     datavol1
```

**2) 匿名ボリュームをアタッチしたコンテナを作成します**
```
$ docker container run --rm --name demo1 -ti -v /data busybox

/ #
```

**3) コンテナでファイルを作成します**
```
/ # echo sample > /data/file.txt
/ # cat /data/file.txt

sample
```

**4) もう1つ端末を開き、ログインしてボリュームリストを確認します。匿名ボリュームが作成されています。**
```
$ docker volume ls

DRIVER    VOLUME NAME
local         6f8ec82139165820db09b611a9fe074c29a5500819892706ccb07e83913412da
local         4ce3647f14b2ca10791eb8a305960d331cd16cca6ec6e8981fa4d4ceb3b81e7e
local         datavol1
```

**5) コンテナの端末に戻りexitしてコンテナを終了します。ボリュームを確認するとコンテナで使っていた匿名ボリュームは消えています。**
```
/ # exit

$ docker volume ls

DRIVER    VOLUME NAME
local     6f8ec82139165820db09b611a9fe074c29a5500819892706ccb07e83913412da
local     datavol1
```

**6) 名前付きボリュームをアタッチしたコンテナを作成します**
```
$ docker container run --rm --name demo2 -ti -v named_volume:/data busybox

/ #
```

**7) コンテナでファイルを作成します**
```
/ # echo sample > /data/file.txt
/ # cat /data/file.txt

Sample
```

**8) コンテナからexitして、ボリュームを確認します。コンテナで使っていた名前付きボリュームは残っています。**
```
/ # exit

$ docker volume ls

DRIVER    VOLUME NAME
local     6f8ec82139165820db09b611a9fe074c29a5500819892706ccb07e83913412da
local     datavol1
local     named_volume
```

**9) コンテナで作成したファイルを確認します**
```
$ sudo cat /var/lib/docker/volumes/named_volume/_data/file.txt
[sudo] password for student:

Sample
```

**10) 外部ボリューム用のディレクトリを作成してhostsファイルをコピーしておきます**
```
$ mkdir ~/code
$ sudo cp /etc/hosts ~/code/
```

**11) 外部ボリュームをbind mountしたコンテナを作成します**
```
$ docker container run --rm --name demo3 -ti -v /home/student/code:/data busybox

/ #
```

**12) コンテナで /data を確認します**
```
/ # ls /data

hosts

/ # cat /data/hosts

127.0.0.1 localhost

# The following lines are desirable for IPv6 capable hosts
::1 ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts

10.0.0.20       set0-cp
```

**13) コンテナでファイルを作成します。その後exitします。**
```
/ # echo sample > /data/sample.txt
/ # cat /data/sample.txt

sample

/ # exit
```

**14) 外部ボリューム用ディレクトリを確認します。コンテナで作成したファイルも確認しましょう。**
```
$ ls code

hosts  sample.txt

$ cat code/sample.txt

Sample
```

<br>
<br>
以上でこのラボは終了です。
<br>
<br>
[Top](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29)

