## Task3<br>イメージを操作する  

**1) イメージリストの表示**
```
$ docker images

REPOSITORY        TAG        IMAGE ID       CREATED       SIZE
ubuntu            latest     ba6acccedd29   5 days ago    72.8MB
nginx             latest     87a94228f133   9 days ago    133MB
```

**2) イメージの削除。エラーがでる場合、コンテナで使用中なのでコンテナを削除しましょう。又は -f オプションで強制的に削除も可能です。**
```
$ docker image rm ubuntu

Untagged: ubuntu:latest
Untagged: ubuntu@sha256:626ffe58f6e7566e00254b638eb7e0f3b11d4da9675088f4781a50ae288f3322
Deleted: sha256:ba6acccedd2923aee4c2acc6a23780b14ed4b8a5fa4e14e252a23b846df9b6c1
Deleted: sha256:9f54eef412758095c8079ac465d494a2872e02e90bf1fb5f12a1641c0d1bb78b
```

**3) イメージの検索**
```
$ docker search library

NAME                                              DESCRIPTION                                     STARS     OFFICIAL
rancher/library-traefik                                                                           3
rancher/library-busybox                                                                           0
rancher/library-nginx                                                                             0
ponylang/library-documentation-action-v2                                                          0
ponylang/library-documentation-action                                                             0
...
```

**4) イメージ名を指定して検索してみます**
```
$ docker search ssh

NAME                         DESCRIPTION                                     STARS     OFFICIAL
circleci/sshd                Created and managed by https://github.com/ci…   4
rancher/ssh-host-container                                                   1
jenkins/ssh-agent            Docker image for Jenkins agents connected ov…   52
atlassian/ssh-ubuntu                                                         5
jenkins/ssh-slave            A Jenkins slave using SSH to establish conne…   39
linuxkit/sshd                LinuxKit sshd package                           10
docksal/ssh-agent            SSH agent service image for Docksal - http:/…   3
....
```
参考：ImageのTagまで表示したい時は以下のようなcurlコマンドで取得できます。  
```
$ curl https://hub.docker.com/v2/repositories/library/ubuntu/tags |python3 -m json.tool | grep \"name\":

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 20698    0 20698    0     0  41562      0 --:--:-- --:--:-- --:--:-- 41478
            "name": "latest",
            "name": "rolling",
            "name": "mantic-20230520",
            "name": "mantic",
            "name": "lunar-20230522",
            "name": "lunar",
            "name": "jammy-20230522",
            "name": "jammy",
            "name": "devel",
            "name": "bionic-20230530",
```

**5) イメージをダウンロードします**
```
$ docker image pull fedora

Using default tag: latest
latest: Pulling from library/fedora
70fb9965a23f: Pull complete
Digest: sha256:9d71386ce04de67b68519d55ca3926cb960a7768d631e6d215eca69b6bb7c2a3
Status: Downloaded newer image for fedora:latest
docker.io/library/fedora:latest
```

**6) ダウンロードできたか確認します**
```
$ docker images

REPOSITORY        TAG        IMAGE ID       CREATED       SIZE
fedora            latest     0e58495b280f   2 weeks ago   178MB
```

**7) 参考：イメージをアップロードします。**
```
Docker Hubにアカウントを作成しておく必要があります。

$ docker login

Authenticating with existing credentials...
WARNING! Your password will be stored unencrypted in /home/student/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store
Login Succeeded

```

**8) 参考：ImageのTagを変更します。<ユーザー名>/<Image名>**
```
$ docker image tag fedora <ユーザー名>/fedora-new

$ docker images

REPOSITORY               TAG        IMAGE ID       CREATED       SIZE
<ユーザー名>/fedora-new   latest     0e58495b280f   2 weeks ago  178MB
```

**9) 参考：Docker Hubにpushします**
```
$ docker image push <ユーザー名>/fedora-new

Using default tag: latest
The push refers to repository [docker.io/<ユーザー名>/fedora-new]
b6d0e02fe431: Mounted from library/fedora
latest: digest: sha256:b1f95b4bf817417973513dff2fc70974f5f7792eb56506c8b992e49a98fe5c6d size: 529
```

**10) 参考：searchで確認してみます。ブラウザ経由でも確認してみましょう。**
```
$ docker search fedora-new

NAME                      DESCRIPTION   STARS     OFFICIAL   AUTOMATED
denat/fedora-new                        0
<ユーザー名>/fedora-new                  0
```

**11)イメージのsave/load  
busybox imageをpullしておきます**
```
$ docker pull busybox

Using default tag: latest
latest: Pulling from library/busybox
Digest: sha256:f7ca5a32c10d51aeda3b4d01c61c6061f497893d7f6628b92f822f7117182a57
Status: Image is up to date for busybox:latest
docker.io/library/busybox:latest
```

**12) saveします**
```
$ docker image save busybox | gzip -c > busybox-save.tgz
```

**13) 作成されたtarファイルを確認してみます**
```
$ tar tvtf busybox-save.tgz

-rw-r--r-- 0/0  1456 2021-09-14 01:20 16ea53ea7c652456803632d67517b78a4f9075a10bfdc4fc6b7b4cbf2bc98497.json
drwxr-xr-x 0/0  0 2021-09-14 01:20 4775639832413f9c8c12a59a371ed1c5ae79cb5d743bb81266209c0620c4bf5b/
-rw-r--r-- 0/0  3 2021-09-14 01:20 4775639832413f9c8c12a59a371ed1c5ae79cb5d743bb81266209c0620c4bf5b/VERSION
-rw-r--r-- 0/0  1133 2021-09-14 01:20 4775639832413f9c8c12a59a371ed1c5ae79cb5d743bb81266209c0620c4bf5b/json
-rw-r--r-- 0/0  1454592 2021-09-14 01:20 4775639832413f9c8c12a59a371ed1c5ae79cb5d743bb81266209c0620c4bf5b/layer.tar
-rw-r--r-- 0/0  203 1970-01-01 00:00 manifest.json
-rw-r--r-- 0/0  90 1970-01-01 00:00 repositories
```

**14) busybox imageを削除します**
```
$ docker image rm -f busybox

Untagged: busybox:latest
Untagged: busybox@sha256:f7ca5a32c10d51aeda3b4d01c61c6061f497893d7f6628b92f822f7117182a57

```

**15) loadします**
```
$ docker image load -i busybox-save.tgz

Loaded image: busybox:latest
```

**16) Imageがloadされた事を確認します**
```
$ docker images

REPOSITORY        TAG       IMAGE ID       CREATED       SIZE
busybox           latest    16ea53ea7c65   5 weeks ago   1.24MB
```

**17) イメージの変更とコミット  
Ubuntuを使ってコンテナを起動します。名前をc1として会話型シェルを起動します。**
```
$ docker container run --name c1 -ti ubuntu
```

**18) curlがインストールされていない事を確認します。**
```
root@52076d31dec2:/# curl -V

bash: curl: command not found
```

**19) curlをインストールします。Update後installします。**
```
root@52076d31dec2:/# apt-get update

root@52076d31dec2:/# apt-get install -y curl

Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:

```

**20) curlがInstallされました。確認します。**
```
root@52076d31dec2:/# curl -V

curl 7.68.0 (x86_64-pc-linux-gnu) libcurl/7.68.0 OpenSSL/1.1.1f zlib/1.2.11 brotli/1.0.7 libidn2/2.2.0 libpsl/0.21.0 (+libidn2/2.2.0) libssh/0.9.3/openssl/zlib nghttp2/1.40.0 librtmp/2.3
Release-Date: 2020-01-08
Protocols: dict file ftp ftps gopher http https imap imaps ldap ldaps pop3 pop3s rtmp rtsp scp sftp smb smbs smtp smtps telnet tftp
Features: AsynchDNS brotli GSS-API HTTP2 HTTPS-proxy IDN IPv6 Kerberos Largefile libz NTLM NTLM_WB PSL SPNEGO SSL TLS-SRP UnixSockets
```

**21) クリーンアップしてexitします**
```
root@52076d31dec2:/# apt-get clean
root@52076d31dec2:/# history -c
root@52076d31dec2:/# exit
```

**22) 変更をdocker diffで確認してみましょう**
```
$ docker diff c1

C /root
A /root/.bash_history
C /var
C /var/log
C /var/log/apt
C /var/log/apt/history.log
...
```

**23) 変更をCommitします。Tagを設定してubuntu:curlという名前にします。**
```
$ docker commit -a "Name" -m "Add curl" c1 ubuntu:curl

sha256:d0e87e6565666db6314380ce0bf72065c9cd51580897716e512bebd5d6e960ce
```

**24) Imageを確認します**
```
$ docker images

REPOSITORY        TAG       IMAGE ID        CREATED          SIZE
ubuntu            curl      d0e87e656566    8 seconds ago    120MB
ubuntu            latest    ba6acccedd29    5 days ago       72.8MB
```

**25) 新しいImageでコンテナを作成してみます。curlが使える事を確認します。**
```
$ docker run --name c2 -ti ubuntu:curl

root@f2a3d3365b00:/# curl -V

curl 7.68.0 (x86_64-pc-linux-gnu) libcurl/7.68.0 OpenSSL/1.1.1f zlib/1.2.11 brotli/1.0.7 libidn2/2.2.0 libpsl/0.21.0 (+libidn2/2.2.0) libssh/0.9.3/openssl/zlib nghttp2/1.40.0 librtmp/2.3
Release-Date: 2020-01-08
Protocols: dict file ftp ftps gopher http https imap imaps ldap ldaps pop3 pop3s rtmp rtsp scp sftp smb smbs smtp smtps telnet tftp
Features: AsynchDNS brotli GSS-API HTTP2 HTTPS-proxy IDN IPv6 Kerberos Largefile libz NTLM NTLM_WB PSL SPNEGO SSL TLS-SRP UnixSockets
```

**26) コンテナからexitします**
```
root@f2a3d3365b00:/# exit
```
<br>
<br>
以上でこのラボは終了です。　　

<br>
[Top](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29)
