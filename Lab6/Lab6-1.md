## Lab6 Kubernetes Install 

**時間:60分**

**目的:**

このラボを完了すると、次のことができるようになります。  
•	kubeadmを使用したkubernetesクラスターのインストール

**Notices:**

•	cp NodeはDocker Labで使っていたVMなのでDocker Install済みです。  
•	Worker NodeはDocker Installから実施します。  

  
<br>
<br>

## Task1<br>Kubernetes Cluster cp Node Setup  

**1) rootにスイッチします**
```
$ sudo -i
[sudo] password for student:
#
```

**2) swap を無効化していない場合は、無効化してください。クラウドプロバイダーは、swap をイメージ内で無効化しています。**
```
# swapoff -a
```

**3) 必要なカーネルモジュールが利用可能になるよう、ロードします。**
```
# modprobe overlay
# modprobe br_netfilter

```

**4) Kernel のネットワークを、必要なトラフィックを許可するように更新します。**
```
# cat << EOF | tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
```

**5) 変更が現在のカーネルでも利用されるようにします。**
```
# sysctl --system


* Applying /etc/sysctl.d/10-console-messages.conf ...
kernel.printk = 4 4 1 7
* Applying /etc/sysctl.d/10-ipv6-privacy.conf ...
net.ipv6.conf.all.use_tempaddr = 2
net.ipv6.conf.default.use_tempaddr = 2
* Applying /etc/sysctl.d/10-kernel-hardening.conf ...
kernel.kptr_restrict = 1
< 省略 >
```

**6) ソフトウェアのインストールに必要な鍵をインストールします**
```
# mkdir -p /etc/apt/keyrings

# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

# echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


```

**7) 通常はこのタイミングでcontainerdのInstallが必要ですが、このVMはInstall済みなので設定変更のみを行います**
```
# containerd config default | tee /etc/containerd/config.toml

# sed -e 's/SystemdCgroup = false/SystemdCgroup = true/g' -i /etc/containerd/config.toml

# systemctl restart containerd

```


**8) aptのパッケージ一覧を更新し、Kubernetesのaptリポジトリを利用するのに必要なパッケージをインストールします。**
```
# apt-get install -y apt-transport-https ca-certificates curl gpg
```

**9) Kubernetesパッケージリポジトリーの公開署名キーをダウンロードします。**
```
# curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor \
-o /etc/apt/keyrings/kubernetes-apt-keyring.gpg


```

**10) 適切なKubernetes aptリポジトリを追加します。このリポジトリは、Kubernetes 1.29用のパッケージのみであることに注意してください。**
```
# echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

```

**11) 記述したリポジトリを更新します。これにより、新しく追加したリポジトリ情報をダウンロードするようになります。**
```
# apt-get update

Hit:1 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal InRelease
Hit:2 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal-updates InRelease
Hit:3 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal-backports InRelease
Hit:5 http://security.ubuntu.com/ubuntu focal-security InRelease
Get:4 https://packages.cloud.google.com/apt kubernetes-xenial InRelease [9383 B]
Get:6 https://packages.cloud.google.com/apt kubernetes-xenial/main amd64 Packages [50.9 kB]
Fetched 60.3 kB in 1s (41.7 kB/s)
Reading package lists... Done

```
[apt-get updateでエラーが出た場合](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab6/labfiles/apt-get-lock-error.md?ref_type=heads)

<br>

**12) kubeadm, kubelet, kubectl ソフトウェアをインストールします。**
```
# apt-get install -y kubelet kubeadm kubectl

Reading package lists... Done
Building dependency tree
Reading state information... Done
...
```

**13) バージョンを固定します**
```
# apt-mark hold kubelet kubeadm kubectl

kubelet set on hold.
kubeadm set on hold.
kubectl set on hold.
```

**14) 7hostsファイルのsetXX-cpにk8scpエイリアスを追加します**
```
# vi /etc/hosts

例：
10.0.0.30       set99-cp k8scp
```

**15) kubaadm-config.yamlを作成します。kubernetesVersionはインストールしたkubeadmのバージョンと同じにします。**
```
# vi kubeadm-config.yaml
```
[kubaadm-config.yaml](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab6/labfiles/kubaadm-config.yaml)

**16) Clusterを構成します。ログを保存するためteeを利用します。**
```
# kubeadm init --config=kubeadm-config.yaml --upload-certs | tee kubeadm-init.out

[init] Using Kubernetes version: v1.29.1
[preflight] Running pre-flight checks
[preflight] Pulling images required for setting up a Kubernetes cluster
[preflight] This might take a minute or two, depending on the speed of your internet connection
[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
W0327 11:03:01.787934   45481 checks.go:835] detected that the sandbox image "registry.k8s.io/pause:3.6" 
<省略>

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of the control-plane node running the following command on each as root:

  kubeadm join k8scp:6443 --token cxup0p.34rm64ihjyphxshp \
        --discovery-token-ca-cert-hash sha256:e34b162092f0d687da6c714885cbcf9d7592d68848a4782edf3a9555b4d44b09 \
        --control-plane --certificate-key 79e68b2a4cdbeb639cf1d9a2af8f076cf4d1263c246b7dc37e3dc49b116298cd

Please note that the certificate-key gives access to cluster sensitive data, keep it secret!
As a safeguard, uploaded-certs will be deleted in two hours; If necessary, you can use
"kubeadm init phase upload-certs --upload-certs" to reload certs afterward.

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join k8scp:6443 --token cxup0p.34rm64ihjyphxshp \
        --discovery-token-ca-cert-hash sha256:e34b162092f0d687da6c714885cbcf9d7592d68848a4782edf3a9555b4d44b09

```
この後実施するコマンドが表示されています。また、最後に表示されるJoinコマンドをメモしておいてください。ワーカーノードの追加で使用します。
<br>

**17) rootユーザーから抜けます**
```
# exit

logout
```

**18) studentアカウントでクラスター管理ができるように設定を行います。これはログに含まれています。**
```
$ mkdir -p $HOME/.kube
$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

**19) Calicoネットワークプラグインの設定ファイルをダウンロードします**
```
$ wget https://raw.githubusercontent.com/projectcalico/calico/v3.25.0/manifests/calico.yaml


Resolving gitlab.com (gitlab.com)... 172.65.251.78, 2606:4700:90:0:f22e:fbec:5bed:a9b9
Connecting to gitlab.com (gitlab.com)|172.65.251.78|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 235185 (230K) [text/plain]
Saving to: ‘calico.yaml’

calico.yaml                      [==========================>] 229.67K  1.25MB/s    in 0.2s

2023-02-13 14:45:34 (1.25 MB/s) - ‘calico.yaml’ saved [235185/235185]
```

**20) calicoをインストールします**
```
$ kubectl apply -f calico.yaml

configmap/calico-config created
customresourcedefinition.apiextensions.k8s.io/bgpconfigurations.crd.projectcalico.org created
...
serviceaccount/calico-kube-controllers created
Warning: policy/v1beta1 PodDisruptionBudget is deprecated in v1.21+, unavailable in v1.25+; use policy/v1 PodDisruptionBudget
poddisruptionbudget.policy/calico-kube-controllers created
```

**21) Podが1/1で起動している事を確認します。全て起動するまで数分待つかもしれません。**
```
$ kubectl get pods --all-namespaces

NAMESPACE     NAME                                      READY    STATUS      RESTARTS   AGE
kube-system   calico-kube-controllers-5d995d45d6-g2qzw  1/1      Running     0          45s
kube-system   calico-node-8rhbc                         1/1      Running     0          45s
kube-system   coredns-558bd4d5db-llmt5                  1/1      Running     0          8m44s
kube-system   coredns-558bd4d5db-p8dck                  1/1      Running     0          8m44s
kube-system   etcd-set99-cp                         1/1      Running     0          8m58s
kube-system   kube-apiserver-set99-cp               1/1      Running     0          8m58s
kube-system   kube-controller-manager-set99-cp      1/1      Running     0          8m58s
kube-system   kube-proxy-qhcvb                          1/1      Running     0          8m44s
kube-system   kube-scheduler-set99-cp               1/1      Running     0          8m58s
```

**22) NodeがReadyである事を確認します**
```
$ kubectl get nodes

NAME           STATUS   ROLES                  AGE    VERSION
set99-cp   Ready    control-plane,cp   10m    v1.29.3
```

**23) bashの自動補完機能を設定します**
```
$ sudo apt-get install bash-completion -y

Reading package lists... Done
Building dependency tree
Reading state information... Done
bash-completion is already the newest version (1:2.10-1ubuntu1).
bash-completion set to manually installed.
0 upgraded, 0 newly installed, 0 to remove and 3 not upgraded.
```

**24) ログアウトしてログインし直します。その後以下を実行します。**
```
$ exit

$ source <(kubectl completion bash)
$ echo "source <(kubectl completion bash)" >> $HOME/.bashrc

自動補完の確認をしましょう
kubectl des<Tab> n<Tab><Tab>
```
<br>
<br>

以上でコントロールプレーンノードのセットアップが完了しました。  
<br>
[Next](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab6/Lab6-2.md)  

[Top](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29)  


