## Task2<br>Kubernetes Cluster Worker Node Setup  

**1) ワーカーノードにログインしてrootにスイッチします**
```
worker:~$ sudo -i
[sudo] password for student:
```

**2) OSをアップデートします**
```
worker:~# apt-get update && apt-get upgrade -y
```

**3) コントロールプレーンノードと同様の手順で必要なソフトウェア及びcontainerdをインストールします。**
```
worker:~# apt install curl apt-transport-https vim git wget gnupg2 \
software-properties-common ca-certificates uidmap -y

worker:~# swapoff -a

worker:~# modprobe overlay

worker:~# modprobe br_netfilter

worker:~# cat << EOF | tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF


worker:~# sysctl --system

worker:~# mkdir -p /etc/apt/keyrings

worker:~# curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
| sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

worker:~# echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


worker:~# apt-get update && apt-get install containerd.io -y

worker:~# containerd config default | tee /etc/containerd/config.toml

worker:~# sed -e 's/SystemdCgroup = false/SystemdCgroup = true/g' -i /etc/containerd/config.toml

worker:~# systemctl restart containerd


```

**4) aptのパッケージ一覧を更新し、Kubernetesのaptリポジトリを利用するのに必要なパッケージをインストールします。**
```
worker:~# apt-get install -y apt-transport-https ca-certificates curl gpg
```

**5) Kubernetesパッケージリポジトリーの公開署名キーをダウンロードします。**
```
worker:~# curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor \
-o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

```
**6) 適切なKubernetes aptリポジトリを追加します**
```
 worker:~# echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

```
**7) 記述したリポジトリを更新します**
```
worker:~# apt-get update

Hit:1 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal InRelease
Hit:2 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal-updates InRelease
Hit:3 http://ap-northeast-1.ec2.archive.ubuntu.com/ubuntu focal-backports InRelease
Hit:5 http://security.ubuntu.com/ubuntu focal-security InRelease
Get:4 https://packages.cloud.google.com/apt kubernetes-xenial InRelease [9383 B]
Get:6 https://packages.cloud.google.com/apt kubernetes-xenial/main amd64 Packages [50.9 kB]
Fetched 60.3 kB in 1s (41.7 kB/s)
Reading package lists... Done
```

**8) kubeadm, kubelet, kubectl ソフトウェアをインストールします**
```
worker:~# apt-get install -y kubelet kubeadm kubectl

Reading package lists... Done
Building dependency tree
Reading state information... Done
...
```

**9) バージョンを固定します**
```
worker:~#  apt-mark hold kubelet kubeadm kubectl

kubelet set on hold.
kubeadm set on hold.
kubectl set on hold.
```

**10) hostsファイルのsetXX-cpにk8scpエイリアスを追加します**
```
worker:~# vi /etc/hosts

例：  
10.0.0.30       set99-cp k8scp  
10.0.0.71       set99-worker  
```

**11) コントロールプレーンノード設定時に表示されたJoinコマンドを実行します**
```
worker:~#  kubeadm join k8scp:6443 --token cxup0p.34rm64ihjyphxshp \
        --discovery-token-ca-cert-hash sha256:e34b162092f0d687da6c714885cbcf9d7592d68848a4782edf3a9555b4d44b09


[preflight] Running pre-flight checks
        [WARNING IsDockerSystemdCheck]: detected "cgroupfs" as the Docker cgroup driver. The recommended driver is "systemd". Please follow the guide at https://kubernetes.io/docs/setup/cri/
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.

```

**12) rootユーザーから抜けます**
```
worker:~# exit

logout
```

**13) コントロールプレーンノードに戻りクラスターノードが追加されてReadyになっている事を確認します。数分待つかもしれません。**
```
cp:~$ kubectl get nodes

NAME           STATUS   ROLES                  AGE    VERSION
set99-cp       Ready    control-plane,cp       35m    v1.29.3
set99-worker   Ready    <none>                 75s    v1.29.3
```

**14) この後の演習のためTaintを削除します**
```
cp:~$ kubectl describe node | grep -i taint

Taints:             node-role.kubernetes.io/cp:NoSchedule
Taints:             <none>

cp:~$ kubectl taint nodes --all node-role.kubernetes.io/control-plane:NoSchedule-

node/set99-cp untainted
error: taint "node-role.kubernetes.io/control-plane:NoSchedule" not found

cp:~$ kubectl describe node | grep -i taint

Taints:             <none>
Taints:             <none>
```

**15) セットアップは完了しました。動作確認しましょう。**
```
cp:~$ kubectl run whaleme --image=docker/whalesay --restart=Never --rm -it  -- sh -c "cowsay Hello!"

最初の実行時に以下が表示されたらEnterキーで抜けて再度実行してみてください。
If you don't see a command prompt, try pressing enter.


cp:~$ kubectl run whaleme --image=docker/whalesay --restart=Never --rm -it  -- sh -c "cowsay -f elephant Hello!"
cp:~$ kubectl run whaleme --image=docker/whalesay --restart=Never --rm -it  -- sh -c "cowsay -f dragon-and-cow Hello!"
```
<br>
<br>
以上でこのラボは終了です。  

<br>

[Top](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29)
