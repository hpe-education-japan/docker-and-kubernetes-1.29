## Task2<br>hostpathを使う  

**1) hostpathで使用するdirectoryを作成します**
```
cp:~$ mkdir data
```

**2) hostpathを使用するマニフェストを作成します**
```
cp:~$ vi hostpath.yaml
```
[hostpath.yaml](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab9/labfiles/hostpath.yaml)

**3) Podを作成します**
```
cp:~$ kubectl apply -f hostpath.yaml

pod/nginx-hostpath created
```

**4) Podのステータスを確認します**
```
cp:~$ kubectl get pods

NAME                        READY   STATUS              RESTARTS   AGE
nginx-hostpath              0/1     ContainerCreating   0          3s
```

**5) ContainerCreatingのままのようです。Podの詳細を見てみます。**
```
cp:~$ kubectl describe pod nginx-hostpath

Name:         nginx-hostpath
Namespace:    default
Priority:     0
Node:         set99-worker/10.0.0.71
Start Time:   Thu, 11 Nov 2021 13:53:24 +0900

Events:
  Type     Reason       Age                 From               Message
  ----     ------       ----                ----               -------
  Normal   Scheduled    3m2s                default-scheduler  Successfully assigned default/nginx-hostpath to set99-worker
  Warning  FailedMount  59s                 kubelet            Unable to attach or mount volumes: unmounted volumes=[hostpath-volume], unattached volumes=[hostpath-volume kube-api-access-wv7cc]: timed out waiting for the condition
  Warning  FailedMount  54s (x9 over 3m2s)  kubelet            MountVolume.SetUp failed for volume "hostpath-volume" : hostPath type check failed: /home/student/data is not a directory
```
Podがワーカーノードで作成されている事がわかります。directoryはワーカーノードで作成する必要がありました。


**6) いったん作成中のPodを削除します**
```
cp:~$ kubectl delete pod nginx-hostpath

pod "nginx-hostpath" deleted
```

**7) 別端末でワーカーノードにログインしてdirectoryを作成します**
```
worker:~$ mkdir data
```

**8) マスターノードに戻り再度Podを作成します**
```
cp:~$ kubectl apply -f hostpath.yaml

pod/nginx-hostpath created
```

**1) 今度はRunningになりました**
```
cp:~$ kubectl get pods

NAME                        READY   STATUS    RESTARTS   AGE
nginx-hostpath              1/1     Running   0          91s
```

**10) nginx-hostpath Podに接続してmount情報を確認します**
```
cp:~$ kubectl exec -it nginx-hostpath -- /bin/bash

root@nginx-hostpath:/# df

Filesystem     1K-blocks    Used Available Use% Mounted on
overlay          8065444 4773724   3275336  60% /
tmpfs              65536       0     65536   0% /dev
tmpfs            2009144       0   2009144   0% /sys/fs/cgroup
/dev/root        8065444 4773724   3275336  60% /test-data
shm                65536       0     65536   0% /dev/shm
tmpfs            2009144      12   2009132   1% /run/secrets/kubernetes.io/serviceaccount
tmpfs            2009144       0   2009144   0% /proc/acpi
tmpfs            2009144       0   2009144   0% /proc/scsi
tmpfs            2009144       0   2009144   0% /sys/firmware
```

**11) /test-dataにファイルを作成します。その後コンテナからexitします。**
```
root@nginx-hostpath:/# touch /test-data/file

root@nginx-hostpath:/# ls /test-data

file

root@nginx-hostpath:/# exit

exit
```

**12) ワーカーノードでPodで作成したファイルを確認します**
```
worker:~$ ls -la data

total 8
drwxrwxr-x 2 student student 4096 Nov 11 14:02 .
drwxr-xr-x 5 student student 4096 Nov 11 13:58 ..
-rw-r--r-- 1 root    root       0 Nov 11 14:02 file
```

このようにhostpathはPodが起動するノードのdirectoryを使用するので注意が必要です。

<br>
<br>
以上でこのラボは終了です。  
<br>
<br>
[Top](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29)  

