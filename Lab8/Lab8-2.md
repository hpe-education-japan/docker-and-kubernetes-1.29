## Task2<br>Deployment  

**1) Deploymentのマニフェストを作成します。Nginxイメージを使ってレプリカ数を3にします。**
```
cp:~$ vi deployment.yaml
```
[deployment.yaml](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab8/labfiles/deployment.yaml)

**2) Deploymentを作成します**
```
cp:~$ kubectl apply -f deployment.yaml

deployment.apps/frontend created
```

**3) 作成されたDeploymentとReplicaSetとPodの状態を確認します。名前をみてみましょう。**
```
cp:~$ kubectl get deployment

NAME       READY   UP-TO-DATE   AVAILABLE   AGE
frontend   3/3     3            3           5s

cp:~$ kubectl get rs

NAME                  DESIRED   CURRENT   READY   AGE
frontend-66b6c48dd5   3         3         3       8s

cp:~$ kubectl get pods

NAME                        READY   STATUS    RESTARTS   AGE
frontend-66b6c48dd5-2ncxr   1/1     Running   0          5s
frontend-66b6c48dd5-dv992   1/1     Running   0          5s
frontend-66b6c48dd5-vj2wf   1/1     Running   0          5s
```

**4) Deploymentでもスケールを変更できます**
```
cp:~$ kubectl scale deployment frontend --replicas=4

deployment.apps/frontend scaled

cp:~$ kubectl get pods

NAME                        READY   STATUS    RESTARTS   AGE
frontend-66b6c48dd5-2ncxr   1/1     Running   0          6m35s
frontend-66b6c48dd5-dv992   1/1     Running   0          6m35s
frontend-66b6c48dd5-m8bkh   1/1     Running   0          7s
frontend-66b6c48dd5-vj2wf   1/1     Running   0          6m35s
```

**5) Deploymentが管理しているReplicaSetを使ってスケールサイズを変更してみます**
```
cp:~$ kubectl scale rs frontend-66b6c48dd5 --replicas=3

replicaset.apps/frontend-66b6c48dd5 scaled
```

**6) コマンドは実行されましたが、Podの数を見てみましょう。Pod Nameの変化も確認してみて下さい。**
```
cp:~$ kubectl get pods

NAME                        READY   STATUS    RESTARTS   AGE
frontend-66b6c48dd5-2ncxr   1/1     Running   0          8m58s
frontend-66b6c48dd5-dv992   1/1     Running   0          8m58s
frontend-66b6c48dd5-vj2wf   1/1     Running   0          8m58s
frontend-66b6c48dd5-xddzb   1/1     Running   0          76s
```
<br>
何がおこったのでしょうか。  
ReplicaSetはPod数を3に戻すように1つのPodを削除しました。DeploymentはPod数を4に保とうとするので、ReplicaSetのreplicas設定を4にもどしました。  
<br>
<br>
 
**7) Deploymentでレプリカ数を3に戻します**
```
cp:~$ kubectl scale deployment frontend --replicas=3

deployment.apps/frontend scaled
```

**8) コンテナイメージを更新してみます。現在のコンテナイメージを確認します。**
```
cp:~$ kubectl get deployments.apps -o wide

NAME       READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES         SELECTOR
frontend   3/3     3            3           16m   nginx        nginx:1.14.2   app=nginx

cp:~$ kubectl get rs -o wide

NAME                  DESIRED   CURRENT   READY   AGE   CONTAINERS   IMAGES         SELECTOR
frontend-66b6c48dd5   3         3         3       17m   nginx        nginx:1.14.2   app=nginx,pod-template-hash=66b6c48dd5

cp:~$ kubectl describe pod frontend-66b6c48dd5-2ncxr

Name:         frontend-66b6c48dd5-2ncxr
Namespace:    default
...
    Image:          nginx:1.14.2
...
```

**9) CLIでコンテナイメージをnginx:1.20.1に更新します**
```
cp:~$ kubectl set image deployment frontend nginx=nginx:1.20.1

deployment.apps/frontend image updated
```

**10) 更新できたか確認します**
```
cp:~$ kubectl get deployments.apps -o wide

NAME       READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES         SELECTOR
frontend   3/3     3            3           26m   nginx        nginx:1.20.1   app=nginx

cp:~$ kubectl get rs -o wide

NAME                 DESIRED   CURRENT   READY   AGE   CONTAINERS   IMAGES         SELECTOR
frontend-58b9b8ff79  3         3         3       49s   nginx        nginx:1.20.1   app=nginx,pod-template-hash=58b9b8ff79
frontend-66b6c48dd5  0         0         0       26m   nginx        nginx:1.14.2   app=nginx,pod-template-hash=66b6c48dd5
```

ReplicaSetが２つ出来ています。違いはイメージのバージョンとDesiredのPod数です。  
Deploymentはバージョンごとに異なるReplicaSetを作成してPodを管理しています。  
Podの詳細も見ておきましょう。  


```
cp:~$ kubectl get pods

NAME                        READY   STATUS    RESTARTS   AGE
frontend-58b9b8ff79-6gvw7   1/1     Running   0          3m
frontend-58b9b8ff79-d9mhz   1/1     Running   0          3m6s
frontend-58b9b8ff79-ttf6v   1/1     Running   0          2m58s

cp:~$ kubectl describe pod frontend-58b9b8ff79-6gvw7

Name:         frontend-58b9b8ff79-6gvw7
Namespace:    default
Priority:     0
Node:         set99-worker/10.0.0.71
...
    Image:          nginx:1.20.1
...
```

**11) 更新履歴を表示します**
```
cp:~$ kubectl rollout history deployment frontend

deployment.apps/frontend
REVISION  CHANGE-CAUSE
1                 <none>
2                 <none>
```

**12) それぞれの履歴を確認します**
```
cp:~$ kubectl rollout history deployment frontend --revision=1

deployment.apps/frontend with revision #1
Pod Template:
  Labels:       app=nginx
        pod-template-hash=66b6c48dd5
  Containers:
   nginx:
    Image:      nginx:1.14.2
    Port:       80/TCP
    Host Port:  0/TCP
    Environment:        <none>
    Mounts:     <none>
  Volumes:      <none>

cp:~$ kubectl rollout history deployment frontend --revision=2

deployment.apps/frontend with revision #2
Pod Template:
  Labels:       app=nginx
        pod-template-hash=58b9b8ff79
  Containers:
   nginx:
    Image:      nginx:1.20.1
    Port:       80/TCP
    Host Port:  0/TCP
    Environment:        <none>
    Mounts:     <none>
  Volumes:      <none>
```

**13) ロールバックしてみましょう**
```
cp:~$ kubectl rollout undo deployment frontend

deployment.apps/frontend rolled back
```

**14) イメージを確認します。ReplicaSetの状態はどう変わったでしょうか。**
```
cp:~$ kubectl get deployments.apps -o wide

NAME       READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES         SELECTOR
frontend   3/3     3            3           43m   nginx        nginx:1.14.2   app=nginx

cp:~$ kubectl get rs -o wide

NAME                  DESIRED   CURRENT   READY   AGE   CONTAINERS   IMAGES         SELECTOR
frontend-58b9b8ff79   0         0         0       17m   nginx        nginx:1.20.1   app=nginx,pod-template-hash=58b9b8ff79
frontend-66b6c48dd5   3         3         3       43m   nginx        nginx:1.14.2   app=nginx,pod-template-hash=66b6c48dd5
```

**15) 履歴を確認します。Revision1がRevision3に変更されています。**
```
cp:~$ kubectl rollout history deployment frontend

deployment.apps/frontend
REVISION  CHANGE-CAUSE
2                 <none>
3                 <none>

cp:~$ kubectl rollout history deployment frontend --revision=3

deployment.apps/frontend with revision #3
Pod Template:
  Labels:       app=nginx
        pod-template-hash=66b6c48dd5
  Containers:
   nginx:
    Image:      nginx:1.14.2
    Port:       80/TCP
    Host Port:  0/TCP
    Environment:        <none>
    Mounts:     <none>
  Volumes:      <none>
```

<br>
<br>
[Next](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab8/Lab8-3.md)  

[Top](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29)  

