## Task2<br>ポートを開放する  

**1) 設定できるポートの範囲を見てみます**
```
$ cat /proc/sys/net/ipv4/ip_local_port_range

32768   60999
```

**2) httpdイメージでコンテナを起動します。-Pオプションでポートを開放します。**
```
$ docker run --rm -d -P --name web httpd

Unable to find image 'httpd:latest' locally
latest: Pulling from library/httpd
7d63c13d9b9b: Pull complete
ca52f3eeea66: Pull complete
448256567156: Pull complete
21d69ac90caf: Pull complete
462e88bc3074: Pull complete
Digest: sha256:f70876d78442771406d7245b8d3425e8b0a86891c79811af94fb2e12af0fadeb
Status: Downloaded newer image for httpd:latest
6078e70d61161c8b6886229636a580b3b12dec4e9d06e75d34aac0bfb13a4461
```

**3) ポートを確認します**
```
$ docker port web

80/tcp -> 0.0.0.0:49153
80/tcp -> :::49153
```

**4) docker psコマンドも見てみます**
```
$ docker ps

CONTAINER ID   IMAGE     COMMAND              CREATED         STATUS         PORTS                                     NAMES
6078e70d6116   httpd     "httpd-foreground"   7 seconds ago   Up 6 seconds   0.0.0.0:49153->80/tcp, :::49153->80/tcp   web
```

**5) 開放されたポート番号に接続してみます**
```
$ curl http://127.0.0.1:49153

<html><body><h1>It works!</h1></body></html>
```

**6) 外部IPを使ってブラウザからも見てみましょう**
```
http://< VMの外部IPアドレス>:49153  
```
![Lab5-1](/uploads/58a37890fcbda05690d730582a65ff54/Lab5-1.jpg)
<br>

**7) 作成したコンテナを削除しておきましょう**
```
$ docker rm -f $(docker ps -q)
```

<br>
<br>
これでこのラボは終了です。
<br>
<br>
[Top](https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29)

