## Lab1 Docker Install

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab1/Lab1.md?ref_type=heads


<br>  

---
## Lab2-Task1 コンテナとイメージの操作


https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab2/Lab2-1.md?ref_type=heads

## Lab2-Task2 既存のコンテナに接続する

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab2/Lab2-2.md?ref_type=heads

## Lab2-Task3 イメージを操作する

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab2/Lab2-3.md?ref_type=heads

<br>  

---
## Lab3-Task1 Dockerfileを使ったイメージビルドとレイヤー、キャッシュの確認

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab3/Lab3-1.md?ref_type=heads

## Lab3-Task2 Dockerfileを使う

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab3/Lab3-2.md?ref_type=heads

## Lab3-Task3 httpdを起動する

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab3/Lab3-3.md?ref_type=heads

<br>  

---
## Lab4-Task1 名前付きボリュームの作成

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab4/Lab4-1.md?ref_type=heads

## Lab4-Task2 匿名ボリューム、名前付きボリューム、外部ボリューム

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab4/Lab4-2.md?ref_type=heads

<br>  

---
## Lab5-Task1 Private Networkの管理

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab5/Lab5-1.md?ref_type=heads

## Lab5-Task2 ポートを開放する

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab5/Lab5-2.md?ref_type=heads

<br>  

---
## Lab6-Task1 Kubernetes Cluster Master Node Setup

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab6/Lab6-1.md?ref_type=heads

## Lab6-Task1 Kubernetes Cluster Worker Node Setup

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab6/Lab6-2.md?ref_type=heads

<br>  

---
## Lab7-Task1 Podの作成とLabel設定

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab7/Lab7-1.md?ref_type=heads

<br>  

---
## Lab8-Task1 ReplicaSet

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab8/Lab8-1.md?ref_type=heads

## Lab8-Task2 Deployment

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab8/Lab8-2.md?ref_type=heads

## Lab8-Task3 DaemonSet

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab8/Lab8-3.md?ref_type=heads

<br>  

---
## Lab9-Task1 emptyDirを使ったPod内コンテナのファイル共有

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab9/Lab9-1.md?ref_type=heads

## Lab9-Task2 hostpathを使う

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab9/Lab9-2.md?ref_type=heads

<br>  

---
## Lab10-Task1 Serviceを作成する

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab10/Lab10-1.md?ref_type=heads


<br>  

---
## Lab11-Task1 Sock Shop Demo

https://gitlab.com/hpe-education-japan/docker-and-kubernetes-1.29/-/blob/master/Lab11/Lab11-1.md?ref_type=heads




---

